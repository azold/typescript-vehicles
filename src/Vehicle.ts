export abstract class Vehicle {
    public capacity: number;
    public kind: number;
    public speedMax: number;

    constructor(capacity: number, kind: number, speedMax: number) {
        this.capacity = capacity;
        this.kind = kind;
        this.speedMax = speedMax;
    }

    public abstract getTravelTime(): number; 
}
