import { Vehicle } from './Vehicle';
import { CarGear } from './constant/CarGear';
import { DefaultValue } from './constant/DefaultValue';
import { VehicleKind } from './constant/VehicleKind';

export class Car extends Vehicle {  
    private distance: number;
    private gear: number;

    constructor(distance: number) {
        super(DefaultValue.CAR_CAPACITY, VehicleKind.CAR, DefaultValue.CAR_SPEED_MAX);
        
        this.distance = distance;
        this.gear = CarGear.NEUTRAL;
    }

    public getSpeed(): number {                
        return this.speedMax / CarGear.DRIVE_5 * this.gear;
    }

    public gearUp(gear?: number): Car {
        let nextGear: number = this.gear + (gear !== undefined ? gear : 1);
        if (nextGear >  CarGear.DRIVE_5) {
            nextGear = CarGear.DRIVE_5;
        }
        this.gear = nextGear;
        return this;
    }

    public getGear(): number {
        return this.gear;
    }

    public gearDown(gear?: number): Car {
        let nextGear: number = this.gear - (gear !== undefined ? gear : 1);
        if (nextGear <=  CarGear.REVERSE) {
            nextGear = CarGear.REVERSE;
        }
        this.gear = nextGear;
        return this;
    }

    public getTravelTime(): number {
        const hours: number = this.distance / this.getSpeed();
        return Math.round(hours * 1000) / 1000;
    }
}
