import { Vehicle } from './Vehicle';
import { DefaultValue } from './constant/DefaultValue';
import { Location } from './constant/Location';
import { VehicleKind } from './constant/VehicleKind';

export class Aeroplane extends Vehicle {
    public boardingTime: number;
    private location: number;
    private destination: number;

    constructor(location: number) {
        super(DefaultValue.AEROPLANE_CAPACITY,
            VehicleKind.AEROPLANE,
            DefaultValue.AEROPLANE_SPEED_MAX);

        this.location = location;
        this.boardingTime = DefaultValue.AEROPLANE_BOARDING_TIME;
    }

    public getLocation(): number {
        return this.location;
    }

    public setDestination(destination: number): Aeroplane {
        this.destination = destination;
        return this;
    }

    public getDestination(): number {
        return this.destination;
    }

    public getFlightTime(): number {    
        const destination: Location = this.getDestination();
        const location: Location = this.getLocation();
        if (destination === null) {
            return null;
        }
        switch (true) {
            case location === Location.BUD && destination === Location.BTS :
            case location === Location.BTS && destination === Location.BUD :
                return 0.5;
            case location === Location.BUD && destination === Location.PRG :
            case location === Location.PRG && destination === Location.BUD :
                return 1.5;
            case location === Location.BTS && destination === Location.PRG :
            case location === Location.PRG && destination === Location.BTS :
                return 1;                
            default:
                throw new Error('Invalid case');
        }
    }

    public getTravelTime(): number {
        return this.boardingTime + this.getFlightTime();
    }

    public land(): void {
        this.location = this.destination;    
        this.setDestination(null);
    }

}
